package controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

import parsers.CsvParser;
import utils.I18nUtils;
import utils.I18nComparator;

public class I18nController {
	private static CsvParser parser;
	private static I18nUtils utils;
	
	private static final String FRENCH_FILENAME= "I18NFrenchList.csv";
	private static final String MERGED_FILENAME = "I18NListMerged.csv";
	private static final String DOWNLOADS_PATH = System.getProperty("user.dir");
	private static final String[] ORIGINAL_COLUMN_NAMES = {"id", "bundle", "country", "language", "companyID (variant)", "label", "value"};
	private static final String[] FRENCH_COLUMN_NAMES = {"label", "english", "french"};
	private static ArrayList<LinkedHashMap<String, String>> originalCSV;
	private static ArrayList<LinkedHashMap<String, String>> frenchCSV;
	private static ArrayList<LinkedHashMap<String, String>> mergedCSV;
	//=======================================================================================================================================
	public I18nController(){
		parser = new CsvParser();
		utils = new I18nUtils();
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public void generateI18nListWithoutFrench(String filepath){
		originalCSV = parser.parseCSV(filepath, ORIGINAL_COLUMN_NAMES);
		Collections.sort(originalCSV,I18nComparator.sortByLabelAndLanguage());
		//utils.printData(originalCSV);
		
		utils.getI18NsWithoutFrench(originalCSV);
		frenchCSV = utils.getExportData();
		//utils.printData(frenchCSV);
		utils.generateFrenchCSVFile(DOWNLOADS_PATH +  FRENCH_FILENAME, FRENCH_COLUMN_NAMES, frenchCSV);
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public void generateCombinedI18nListWithFrench(String originalCSVPath, String frenchCSVPath){
		originalCSV = parser.parseCSV(originalCSVPath, ORIGINAL_COLUMN_NAMES);
		Collections.sort(originalCSV, I18nComparator.sortByLabelAndLanguage());
		//utils.printData(originalCSV);

		frenchCSV = parser.parseCSV(frenchCSVPath, FRENCH_COLUMN_NAMES);
		Collections.sort(frenchCSV, I18nComparator.sortByLabel());
		//utils.printData(frenchCSV);
		
		mergedCSV = utils.mergeFrenchCSVWithI18nCSV(ORIGINAL_COLUMN_NAMES, originalCSV, frenchCSV);
		utils.generateCSVFile(DOWNLOADS_PATH + MERGED_FILENAME, ORIGINAL_COLUMN_NAMES, mergedCSV);
		utils.printData(mergedCSV);
	}
	//=======================================================================================================================================
}
