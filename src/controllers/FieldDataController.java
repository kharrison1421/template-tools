package controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.javatuples.Pair;

import models.FieldData;
import models.InputType;
import models.Page;
import parsers.XmlParser;
import utils.FieldDataComparator;
import utils.FieldDataUtils;

public class FieldDataController {
	private String exportDirectory = System.getProperty("user.dir") + File.separator;
	// =======================================================================================================================================
	public FieldDataController() {
	}

	public FieldDataController(String exportPath) {
		this.exportDirectory = exportPath;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void reorderFieldDataAndTabs(String directory) {
		Path dir = Paths.get(directory);
		reorderFieldDataAndTabs(dir);
	}

	private void reorderFieldDataAndTabs(Path dir) {
		try {
			DirectoryStream<Path> dStream = Files.newDirectoryStream(dir);
			for (Path entry : dStream) {
				if (Files.isDirectory(entry)) {
					reorderFieldDataAndTabs(entry);
				} else if (entry.getFileName().toString().equals("fieldData.xml")) {
					String filepath = entry.toAbsolutePath().toString();
					LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap = XmlParser.parseXML(filepath);

					// FieldDataUtils.printData(fieldDataMap);
					orderFieldDataByCoordinates(fieldDataMap);
					reorderTabOrder(fieldDataMap);
					FieldDataUtils.exportFieldDataXml(fieldDataMap, filepath);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void orderFieldDataByCoordinates(LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap) {
		// Sort Pages
		List<Map.Entry<Page, ArrayList<FieldData>>> entries = new ArrayList<Map.Entry<Page, ArrayList<FieldData>>>(
				fieldDataMap.entrySet());
		Collections.sort(entries, FieldDataComparator.sortPagesByIndex());
		fieldDataMap.clear();
		for (Map.Entry<Page, ArrayList<FieldData>> entry : entries) {
			fieldDataMap.put(entry.getKey(), entry.getValue());
		}

		// Sort FieldData within page
		Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
			Page page = (Page) pair.getKey();
			ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

			Collections.sort(fieldDataList, FieldDataComparator.sortFieldDataByCoordinates());
			fieldDataMap.put(page, fieldDataList);
		}
	}

	private void reorderTabOrder(LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap) {
		int tabCount = 0;
		int divTabCount = 0;

		Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
			ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

			Iterator<FieldData> iterator = fieldDataList.iterator();
			while (iterator.hasNext()) {
				FieldData fData = iterator.next();
				InputType type = fData.getType();

				switch (type) {
				case CHECKBOX:
				case RADIO_BUTTON:
				case TEXT:
				case DROPDOWN:
				case DATE_PICKER:
				case BUTTON:
					if (fData.getVisibility() != "hiddenDoNotPrint")
						FieldDataUtils.setAttributeNode(fData, "tabOrder", tabCount += 5);
					break;
				case LABEL:
				case LINK:
				case BOX:
					break;
				case DIV:
					FieldDataUtils.setAttributeNode(fData, "tabOrder", divTabCount += 5);
					break;
				case IMAGE:
				case ERROR_LIST:
				case VALIDATION_WIDGET:
				case NON_EXISTANT:
					break;
				default:
					break;
				}
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void splitField(String filepath, String commonSubstring, String numberOfFragmentsString,
			String spacingString, String widthString) {
		LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap = XmlParser.parseXML(filepath);
		int maxId = XmlParser.getMaxId();

		Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
			ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

			ListIterator<FieldData> listIterator = fieldDataList.listIterator();
			while (listIterator.hasNext()) {
				FieldData fData = listIterator.next();

				if (fData.getName().toLowerCase().contains(commonSubstring.toLowerCase())) {
					listIterator.remove();

					int numberOfFragments = Integer.parseInt(numberOfFragmentsString);
					int spacing = Integer.parseInt(spacingString);
					double xLeft = fData.getxLeft();
					double width = Double.parseDouble(widthString);
					double fragmentWidth = (width - ((numberOfFragments - 1) * spacing)) / numberOfFragments;

					for (int i = 0; i < numberOfFragments; i++) {
						FieldData fieldFragment = FieldDataUtils.copyFieldDataNode(fData);

						FieldDataUtils.setAttributeNode(fieldFragment, "id", ++maxId);
						FieldDataUtils.setAttributeNode(fieldFragment, "name",
								fData.getName() + Integer.toString(i + 1));
						FieldDataUtils.setChildNode(fieldFragment, "X_LEFT", xLeft);
						FieldDataUtils.setChildNode(fieldFragment, "X_RIGHT", xLeft + fragmentWidth);

						xLeft += (fragmentWidth + spacing);

						listIterator.add(fieldFragment);
					}
				}
			}
		}

		String exportPath = exportDirectory + "fieldData.xml";
		FieldDataUtils.exportFieldDataXml(fieldDataMap, exportPath);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void orderFieldDataByMasterFields(String filepath, String commonSubstring) {
		LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap = XmlParser.parseXML(filepath);

		LinkedHashMap<String, FieldData> masterFields = getMasterFields(fieldDataMap, commonSubstring);
		FieldData masterField = null;
		int sequence = 2;

		Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
			ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

			String currentName;
			String masterName;
			String commonName;
			String parentDiv = "";
			ListIterator<FieldData> listIterator = fieldDataList.listIterator();
			while (listIterator.hasNext()) {
				FieldData fData = listIterator.next();

				currentName = fData.getName();
				commonName = commonSubstring + Integer.toString(sequence);
				if (fData.getType() == InputType.DIV && currentName.contains(commonName)) {
					parentDiv = fData.getId();
				} else if (fData.getParentDiv() != null && fData.getParentDiv().equals(parentDiv)) {
					masterName = currentName.replaceAll("\\d", "");
					masterField = masterFields.get(masterName);

					FieldDataUtils.setChildNode(fData, "X_LEFT", masterField.getxLeft());
					FieldDataUtils.setChildNode(fData, "X_RIGHT", masterField.getxRight());
					FieldDataUtils.setChildNode(fData, "GROUP_X_LEFT", masterField.getxLeft());
					FieldDataUtils.setChildNode(fData, "GROUP_X_RIGHT", masterField.getxRight());
				}
			}
		}
	}

	private LinkedHashMap<String, FieldData> getMasterFields(LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap,
			String commonSubstring) {
		Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
		LinkedHashMap<String, FieldData> masterFields = new LinkedHashMap<String, FieldData>();
		String parentDiv = "";

		outerLoop: while (it.hasNext()) {
			Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
			ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

			ListIterator<FieldData> listIterator = fieldDataList.listIterator();
			while (listIterator.hasNext()) {
				FieldData fData = listIterator.next();

				String name = "";
				// Common substring will be like: AuthorizedPerson1_SIN. Make it like: AuthorizedPerson_SIN.
				commonSubstring = commonSubstring.replaceAll("\\d", "");
				if (fData.getType() == InputType.DIV && fData.getName().contains(commonSubstring + "1")) {
					name = fData.getName().replaceAll("\\d", "");
					parentDiv = fData.getId();
					masterFields.put(name, fData);
				} else if (fData.getParentDiv() != null && fData.getParentDiv().equals(parentDiv)) {
					name = fData.getName().replaceAll("\\d", "");
					masterFields.put(name, fData);
				} else if (fData.getName().contains(commonSubstring + "2")) {
					break outerLoop;
				}
			}
		}

		return masterFields;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void addNodeToCommonFields(String filepath, String commonRegex, String nodePath, String nodeValue) {
		LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap = XmlParser.parseXML(filepath);

		Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
			ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

			ListIterator<FieldData> listIterator = fieldDataList.listIterator();
			while (listIterator.hasNext()) {
				FieldData fData = listIterator.next();
				String fieldName = fData.getName();

				if (fieldName.matches(commonRegex)) {
					FieldDataUtils.addChildNode(fData, nodePath, nodeValue);
				}
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void createDataDictionary(String landingPagesDirectory, String signingTemplatesDirectory) {
		Map<String, TreeSet<String>> landingPages = extractFieldNames(landingPagesDirectory, true);
		Map<String, TreeSet<String>> signingTemplates = extractFieldNames(signingTemplatesDirectory, true);

		Set<String> landingPageFields = new TreeSet<String>();
		Iterator<Entry<String, TreeSet<String>>> it = landingPages.entrySet().iterator();
		while (it.hasNext()) {
			TreeSet<String> fields = it.next().getValue();
			for (String f : fields) {
				landingPageFields.add(f);
			}
		}

		Map<String, LinkedList<Pair<String, String>>> dataDictionary = new LinkedHashMap<String, LinkedList<Pair<String, String>>>();
		for (String landingPageField : landingPageFields) {
			LinkedList<Pair<String, String>> row = new LinkedList<Pair<String, String>>();
			it = signingTemplates.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, TreeSet<String>> entry = it.next();
				String templateName = entry.getKey();
				TreeSet<String> templateFields = entry.getValue();
				Pair<String, String> cell = new Pair<String, String>(templateName, "");
				if (templateFields.contains(landingPageField)) {
					cell = new Pair<String, String>(templateName, "X");
				}
				row.add(cell);
			}
			dataDictionary.put(landingPageField, row);
		}

		String exportPath = exportDirectory + "Data Dictionary.xls";
		FieldDataUtils.exportDataDictionaryXls(dataDictionary, exportPath);
		//FieldDataUtils.printDataDictionary(dataDictionary);
	}

	public void exportFieldNamesList(String directory) {
		String exportPath = exportDirectory + "Field Names.txt";
		Map<String, TreeSet<String>> fieldNames = extractFieldNames(directory, false);
		FieldDataUtils.exportFieldNamesTxt(fieldNames, exportPath);
	}

	public Map<String, TreeSet<String>> extractFieldNames(String directory, boolean includeHiddenFields) {
		Map<String, TreeSet<String>> fieldNameList = new TreeMap<String, TreeSet<String>>();

		Path dir = Paths.get(directory);
		return fieldNameList = traverseDirectories(dir, fieldNameList, includeHiddenFields);
	}

	private Map<String, TreeSet<String>> traverseDirectories(Path dir, Map<String, TreeSet<String>> fieldNameList2,
			boolean includeHiddenFields) {
		try (DirectoryStream<Path> dStream = Files.newDirectoryStream(dir)) {
			for (Path entry : dStream) {
				if (Files.isDirectory(entry)) {
					traverseDirectories(entry, fieldNameList2, includeHiddenFields);
				}

				if (entry.getFileName().toString().equals("fieldData.xml")) {
					String filepath = entry.toAbsolutePath().toString();
					LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap = XmlParser.parseXML(filepath);
					TreeSet<String> fieldNameList = new TreeSet<String>();

					Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
					while (it.hasNext()) {
						Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
						ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

						ListIterator<FieldData> listIterator = fieldDataList.listIterator();
						while (listIterator.hasNext()) {
							FieldData fData = listIterator.next();

							if (includeHiddenFields || !fData.getVisibility().equals("hiddenDoNotPrint")) {
								InputType type = fData.getType();

								switch (type) {
								case CHECKBOX:
								case RADIO_BUTTON:
								case TEXT:
								case DROPDOWN:
								case DATE_PICKER:
									fieldNameList.add(fData.getName());
									break;
								default:
									break;
								}
							}
						}
					}

					String templateDataFilepath = entry.toAbsolutePath().toString().replace("fieldData.xml",
							"templatedata.xml");
					fieldNameList2.put(XmlParser.extractTemplatename(templateDataFilepath), fieldNameList);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return fieldNameList2;
	}
	// =======================================================================================================================================
}