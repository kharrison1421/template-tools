package utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class I18nUtils {
	private FileWriter fw;
	private ArrayList<LinkedHashMap<String, String>> exportData = new ArrayList<LinkedHashMap<String, String>>();

	private static final String SPLIT_DELIMITER = ",";
	private static final String ENGLISH_ABBREVIATION = "en";
	// =======================================================================================================================================
	public void getI18NsWithoutFrench(ArrayList<LinkedHashMap<String, String>> csvData) {
		String currentLanguage;
		String currentLabel;
		String nextLabel;
		LinkedHashMap<String, String> current = csvData.get(0);
		LinkedHashMap<String, String> next = csvData.get(1);

		Collections.sort(csvData, I18nComparator.sortByLabelAndLanguage());

		int i = 0;
		while (i < csvData.size()) {
			currentLanguage = current.get("language").toLowerCase();
			currentLabel = current.get("label").toLowerCase();
			nextLabel = next.get("label").toLowerCase();

			if (!currentLabel.equals(nextLabel)) {
				if (currentLanguage.equals(ENGLISH_ABBREVIATION)) {
					exportData.add(current);
				}

				current = next;
				next = csvData.get(++i);
			} else {
				i++;
				if (i < csvData.size()) {
					current = csvData.get(i);
					next = csvData.get(++i);
				}
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void generateFrenchCSVFile(String filepath, String[] frenchColumnNames,
			ArrayList<LinkedHashMap<String, String>> frenchCSV) {
		try {
			fw = new FileWriter(filepath);

			int i = 0;
			for (; i < (frenchColumnNames.length - 1); i++) {
				fw.append(frenchColumnNames[i] + SPLIT_DELIMITER);
			}
			fw.append(frenchColumnNames[i] + "\n");

			for (LinkedHashMap<String, String> map : frenchCSV) {
				fw.append(map.get("label") + SPLIT_DELIMITER);
				fw.append("\"" + map.get("value") + "\"" + SPLIT_DELIMITER);
				fw.append(" \n");
			}
			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void generateCSVFile(String filepath, String[] columnNames,
			ArrayList<LinkedHashMap<String, String>> csvData) {
		try {
			fw = new FileWriter(filepath);

			int i = 0;
			for (; i < (columnNames.length - 1); i++) {
				fw.append(columnNames[i] + SPLIT_DELIMITER);
			}
			fw.append(columnNames[i] + "\n");

			for (LinkedHashMap<String, String> map : csvData) {
				for (Map.Entry<String, String> entry : map.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue();

					if (key.equals("value"))
						fw.append(value + "\n");
					else
						fw.append(value + SPLIT_DELIMITER);

				}
			}

			fw.flush();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public ArrayList<LinkedHashMap<String, String>> mergeFrenchCSVWithI18nCSV(String[] columnNames,
			ArrayList<LinkedHashMap<String, String>> csvData, ArrayList<LinkedHashMap<String, String>> frenchCSV) {
		// frenchCSV = convertFrenchCSVToExportCSV(columnNames, frenchCSV);

		Collections.sort(csvData, I18nComparator.sortById());

		LinkedHashMap<String, String> newRow;
		int latestId = Integer.parseInt(csvData.get(csvData.size() - 1).get("id"));

		for (LinkedHashMap<String, String> map : frenchCSV) {
			newRow = new LinkedHashMap<String, String>();
			newRow.put("id", Integer.toString(++latestId));
			newRow.put("bundle", "template");
			newRow.put("country", "CA");
			newRow.put("language", "fr");
			newRow.put("companyID (variant)", "604");
			newRow.put("label", map.get("label"));
			newRow.put("value", map.get("french"));

			csvData.add(newRow);
		}

		Collections.sort(csvData, I18nComparator.sortByLabelAndLanguage());

		return csvData;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void printData(ArrayList<LinkedHashMap<String, String>> csvData) {
		String line = "";

		for (LinkedHashMap<String, String> map : csvData) {
			for (Entry<String, String> entry : map.entrySet()) {
				line += entry.getValue() + "|";
			}

			System.out.println(line);
			line = "";
		}

		System.out.println("Number of lines:" + csvData.size());
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public ArrayList<LinkedHashMap<String, String>> getExportData() {
		return exportData;
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public void setExportData(ArrayList<LinkedHashMap<String, String>> exportData) {
		this.exportData = exportData;
	}
	// =======================================================================================================================================
}
