package utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.javatuples.Pair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import models.FieldData;
import models.Page;

public class FieldDataUtils {
	// =======================================================================================================================================
	public static void exportFieldDataXml(LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap, String filepath) {
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();

			Element root = doc.createElement("DOC");
			doc.appendChild(root);

			Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
				Page page = (Page) pair.getKey();
				ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

				Element pageNode = doc.createElement("PAGE");

				NamedNodeMap attributes = page.getAttributes();
				for (int i = 0; i < attributes.getLength(); i++) {
					Node attribute = attributes.item(i);
					pageNode.setAttribute(attribute.getNodeName(), attribute.getNodeValue());
				}
				root.appendChild(pageNode);

				for (FieldData fData : fieldDataList) {
					Element fieldNode = doc.createElement("FIELD");

					attributes = fData.getAttributes();
					for (int i = 0; i < attributes.getLength(); i++) {
						Node attribute = attributes.item(i);
						fieldNode.setAttribute(attribute.getNodeName(), attribute.getNodeValue());
					}

					NodeList children = fData.getChildren();
					for (int i = 0; i < children.getLength(); i++) {
						fieldNode.appendChild(doc.importNode(children.item(i), true));
					}
					pageNode.appendChild(fieldNode);
				}
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);

			System.out.println(filepath + " exported Mother Fucker!");
		} catch (ParserConfigurationException e1) {
			e1.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static void exportFieldNamesTxt(Map<String, TreeSet<String>> fieldNames2, String filepath) {
		try (PrintWriter out = new PrintWriter(filepath)) {

			Iterator<Entry<String, TreeSet<String>>> it = fieldNames2.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, TreeSet<String>> pair = it.next();
				String templateName = pair.getKey();
				TreeSet<String> fieldNames = pair.getValue();

				out.println(templateName);
				out.println("--------------------------------------------------------------------");
				for (String fieldName : fieldNames) {
					out.println(fieldName);
				}
				out.println("====================================================================");
			}

			System.out.println(filepath + " exported Mother Fucker!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static void exportDataDictionaryXls(Map<String, LinkedList<Pair<String, String>>> dataDictionary,
			String filepath) {
		try (HSSFWorkbook workbook = new HSSFWorkbook(); FileOutputStream fileOut = new FileOutputStream(filepath);) {
			HSSFSheet sheet = workbook.createSheet("Master List");

			short rowNumber = 0;
			int cellNumber = 0;

			HSSFRow rowhead = sheet.createRow(rowNumber);
			Iterator<Entry<String, LinkedList<Pair<String, String>>>> it = dataDictionary.entrySet().iterator();
			rowhead.createCell(0).setCellValue("Field Name");
			if (it.hasNext()) {
				Entry<String, LinkedList<Pair<String, String>>> entry = it.next();
				List<Pair<String, String>> templates = entry.getValue();
				for (Pair<String, String> p : templates) {
					rowhead.createCell(++cellNumber).setCellValue(p.getValue0());
				}
			}

			it = dataDictionary.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, LinkedList<Pair<String, String>>> entry = it.next();
				String fieldName = entry.getKey();

				cellNumber = 0;
				HSSFRow row = sheet.createRow(++rowNumber);
				row.createCell(cellNumber).setCellValue(fieldName);
				List<Pair<String, String>> templates = entry.getValue();
				for (Pair<String, String> p : templates) {
					row.createCell(++cellNumber).setCellValue(p.getValue1());
				}
			}

			workbook.write(fileOut);
			System.out.println(filepath + " exported Mother Fucker!");
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static void printFieldData(LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap) {
		Iterator<Entry<Page, ArrayList<FieldData>>> it = fieldDataMap.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Page, ArrayList<FieldData>> pair = it.next();
			Page page = (Page) pair.getKey();
			ArrayList<FieldData> fieldDataList = (ArrayList<FieldData>) pair.getValue();

			System.out.print("<PAGE ");
			NamedNodeMap attributes = page.getAttributes();
			for (int i = 0; i < attributes.getLength(); i++) {
				Node attribute = attributes.item(i);
				System.out.print(attribute.getNodeName() + "=\"" + attribute.getNodeValue() + "\" ");
			}
			System.out.print("> \n");

			for (FieldData fData : fieldDataList) {
				System.out.print("\t<FIELD ");

				attributes = fData.getAttributes();
				for (int i = 0; i < attributes.getLength(); i++) {
					Node attribute = attributes.item(i);
					System.out.print(attribute.getNodeName() + "=\"" + attribute.getNodeValue() + "\" ");
				}
				System.out.print("> \n");

				NodeList fieldChildren = fData.getChildren();
				removeEmptyNodes(fieldChildren);
				printXML(fieldChildren, 2);

				System.out.println("\t</FIELD>");
			}

			System.out.println("</PAGE>");
		}
	}

	public static void printXML(NodeList nodeList, int level) {
		String tabs = "";
		for (int i = 0; i < level; i++) {
			tabs += "\t";
		}

		for (int i = 0; i < nodeList.getLength(); i++) {
			Node child = nodeList.item(i);

			if (child.getNodeType() == Node.ELEMENT_NODE) {
				Node grandChild = child.getFirstChild();

				if (grandChild != null) {
					if (grandChild.getNodeType() == Node.TEXT_NODE) {
						System.out.printf("%s<%s>%s</%s> \n", tabs, child.getNodeName(), grandChild.getTextContent(),
								child.getNodeName());
					} else if (grandChild.getNodeType() == Node.ELEMENT_NODE) {
						System.out.printf("%s<%s> \n", tabs, child.getNodeName());
						printXML(child.getChildNodes(), ++level);
						System.out.printf("%s</%s> \n", tabs, child.getNodeName());
					}
				}
			}
		}
	}
	
	public static void printDataDictionary(Map<String, LinkedList<Pair<String, String>>> dataDictionary) {
		Iterator<Entry<String, LinkedList<Pair<String, String>>>> fieldIterator = dataDictionary.entrySet().iterator();
		while(fieldIterator.hasNext()){
			Entry<String, LinkedList<Pair<String, String>>> fieldEntry = fieldIterator.next();
			System.out.println(fieldEntry.getKey());
			for(Pair<String, String> template : fieldEntry.getValue()){
				System.out.println("\t" + template.toString());
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static void removeEmptyNodes(NodeList nodeList) {
		for (int i = 0; i < nodeList.getLength(); i++) {
			Node child = nodeList.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				Node grandChild = child.getFirstChild();
				if (grandChild != null) {
					if ((grandChild.getNodeType() == Node.TEXT_NODE) && grandChild.getNodeValue().trim().equals("")) {
						child.removeChild(grandChild);
						removeEmptyNodes(child.getChildNodes());
					}
				}
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static FieldData copyFieldDataNode(FieldData original) {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(original);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);

			return (FieldData) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static void setAttributeNode(FieldData fData, String attributeName, int value) {
		setAttributeNode(fData.getAttributes(), attributeName, Integer.toString(value));
	}

	public static void setAttributeNode(FieldData fData, String attributeName, String value) {
		setAttributeNode(fData.getAttributes(), attributeName, value);
	}

	private static void setAttributeNode(NamedNodeMap attributes, String attributeName, String value) {
		Node attribute = attributes.getNamedItem(attributeName);
		attribute.setNodeValue(value);
		attributes.setNamedItem(attribute);
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static void setChildNode(FieldData fData, String nodeName, double value) {
		setChildNode(fData.getChildren(), nodeName, Double.toString(value));
	}

	public static void setChildNode(FieldData fData, String nodeName, String value) {
		setChildNode(fData.getChildren(), nodeName, value);
	}

	private static void setChildNode(NodeList children, String nodeName, String value) {
		for (int i = 0; i < children.getLength(); i++) {
			Node child = children.item(i);
			Node node;
			String name = child.getNodeName();
			if (name.equals(nodeName)) {
				node = child.getFirstChild();
				node.setNodeValue(value);

				break;
			} else if (child.getNodeType() == Node.ELEMENT_NODE) {
				Node grandChild = child.getFirstChild();

				if (grandChild != null) {
					name = grandChild.getNodeName();
					if (grandChild.getNodeName().equals(nodeName)) {
						node = grandChild.getFirstChild();
						node.setNodeValue(value);

						break;
					} else if (grandChild.getNodeType() == Node.ELEMENT_NODE) {
						setChildNode(grandChild.getChildNodes(), nodeName, value);
					}
				}
			}
		}
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static void addChildNode(FieldData fData, String nodePath, String value) {
		addChildNode(fData.getRootNode(), nodePath, value, 0);
	}

	@SuppressWarnings("unchecked")
	private static void addChildNode(Node parent, String nodePath, String value, int level) {
		NodeList nodeList = parent.getChildNodes();
		List<Node> children = new ArrayList<Node>((Collection<? extends Node>) Arrays.asList(nodeList));

		boolean nodeFound = false;
		String[] nodeLevels = nodePath.split("/");
		String targetNode = nodeLevels[nodeLevels.length - 1];
		String elementInNodePath = nodeLevels[level];

		for (int i = 0; i < children.size(); i++) {
			Node child = children.get(i);
			String childName = child.getNodeName();
			if (childName.equals(elementInNodePath)) {
				nodeFound = true;
				if (childName.equals(targetNode)) {
					child.setNodeValue(value);
				}
				break;
			}
		}

		if (!nodeFound) {
			// children = new ArrayList<Node>((Collection<? extends Node>)
			// Arrays.asList(child.getChildNodes()));
			// addChildNode(children, nodePath, value, level++);
		} else {
			for (int i = level; i < nodeLevels.length; i++) {
				// Node newNode = new Node();

			}
		}
	}
	// =======================================================================================================================================
}
