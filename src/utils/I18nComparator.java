package utils;

import java.util.Comparator;
import java.util.LinkedHashMap;

/**
 * Sorts the list by label and then language
 * 
 * @author Kieran
 *
 */
public class I18nComparator {
	// =======================================================================================================================================
	protected static Comparator<LinkedHashMap<String, String>> sortById() {
		return new Comparator<LinkedHashMap<String, String>>() {

			@Override
			public int compare(LinkedHashMap<String, String> first, LinkedHashMap<String, String> second) {
				int id1 = Integer.parseInt(first.get("id"));
				int id2 = Integer.parseInt(second.get("id"));

				return id1 - id2;
			}

		};
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static Comparator<LinkedHashMap<String, String>> sortByLabel() {
		return new Comparator<LinkedHashMap<String, String>>() {

			@Override
			public int compare(LinkedHashMap<String, String> first, LinkedHashMap<String, String> second) {
				String label1 = first.get("label").toLowerCase();
				String label2 = second.get("label").toLowerCase();

				return label1.compareTo(label2);
			}

		};
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static Comparator<LinkedHashMap<String, String>> sortByLabelAndLanguage() {
		return new Comparator<LinkedHashMap<String, String>>() {

			@Override
			public int compare(LinkedHashMap<String, String> first, LinkedHashMap<String, String> second) {
				String label1 = first.get("label").toLowerCase();
				String label2 = second.get("label").toLowerCase();
				int compare;

				if ((compare = label1.compareTo(label2)) != 0) {
					return compare;
				} else {
					String language1 = first.get("language").toLowerCase();
					String language2 = second.get("language").toLowerCase();

					return language1.compareTo(language2);
				}
			}

		};
	}
	// =======================================================================================================================================
}
