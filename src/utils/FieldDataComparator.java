package utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import org.w3c.dom.NamedNodeMap;

import models.FieldData;
import models.Page;

/**
 * Sorts the list by label and then language
 * 
 * @author Kieran
 *
 */
public class FieldDataComparator {
	// =======================================================================================================================================
	public static Comparator<Map.Entry<Page, ArrayList<FieldData>>> sortPagesByIndex() {
		return new Comparator<Map.Entry<Page, ArrayList<FieldData>>>() {

			@Override
			public int compare(Entry<Page, ArrayList<FieldData>> first, Entry<Page, ArrayList<FieldData>> second) {
				NamedNodeMap attributes1 = first.getKey().getAttributes();
				int index1 = Integer.parseInt(attributes1.getNamedItem("index").getNodeValue());

				NamedNodeMap attributes2 = second.getKey().getAttributes();
				int index2 = Integer.parseInt(attributes2.getNamedItem("index").getNodeValue());

				return index1 - index2;
			};

		};
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static Comparator<FieldData> sortFieldDataByCoordinates() {
		return new Comparator<FieldData>() {

			@Override
			public int compare(FieldData first, FieldData second) {
				Double compare1;
				Double compare2;

				if (first.getyTop() == second.getyTop()) {
					compare1 = first.getxLeft();
					compare2 = second.getxLeft();
				} else {
					compare1 = second.getyTop();
					compare2 = first.getyTop();
				}

				if (compare1 < compare2)
					return -1;
				else
					return 1;
			};

		};
	}
	// =======================================================================================================================================
}
