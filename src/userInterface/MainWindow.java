package userInterface;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import controllers.FieldDataController;
import controllers.I18nController;

public class MainWindow {

	private Shell shlInTools;
	private I18nController i18nController = new I18nController();
	private FieldDataController fieldController = new FieldDataController();
	private String xmlFilepath;

	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			MainWindow window = new MainWindow();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shlInTools.open();
		shlInTools.layout();
		while (!shlInTools.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlInTools = new Shell();
		shlInTools.setSize(548, 357);
		shlInTools.setText("Template Tools of Ultimate Power");
		shlInTools.setLayout(new GridLayout(11, false));
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Label lblIn = new Label(shlInTools, SWT.NONE);
		lblIn.setText("i18n");
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Label label_1 = new Label(shlInTools, SWT.SEPARATOR | SWT.HORIZONTAL);
		label_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 10, 1));
		new Label(shlInTools, SWT.NONE);
		
		Label lblGetInsThat = new Label(shlInTools, SWT.NONE);
		lblGetInsThat.setText("Get i18ns that need French translation:");
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Button btnGenerateCsv = new Button(shlInTools, SWT.NONE);
		btnGenerateCsv.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnGenerateCsv.setText("Generate CSV");
		btnGenerateCsv.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(shlInTools, SWT.OPEN);
				dialog.setFilterExtensions(new String[] {"*.csv"});
				
				String result = dialog.open();
				if(result != null)
					i18nController.generateI18nListWithoutFrench(result);
			}
		});
		new Label(shlInTools, SWT.NONE);
		
		Label lblAddTranslatedFrench = new Label(shlInTools, SWT.NONE);
		lblAddTranslatedFrench.setText("Add translated i18ns to existing ones:");
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Button btnMergeCsvs = new Button(shlInTools, SWT.NONE);
		btnMergeCsvs.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnMergeCsvs.setText("Merge CSVs");
		btnMergeCsvs.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(shlInTools, SWT.OPEN);
				dialog.setFilterExtensions(new String[] {"*.csv"});
				
				String originalCSVPath = dialog.open();				
				String frenchCSVPath = dialog.open();
				
				if( (originalCSVPath != null) && (frenchCSVPath != null) )
					i18nController.generateCombinedI18nListWithFrench(originalCSVPath, frenchCSVPath);
			}
		});
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Label lblFlex = new Label(shlInTools, SWT.NONE);
		lblFlex.setText("Flex");
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Label label = new Label(shlInTools, SWT.SEPARATOR | SWT.HORIZONTAL);
		label.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 10, 1));
		new Label(shlInTools, SWT.NONE);
		
		Label lblFixDivTab = new Label(shlInTools, SWT.NONE);
		lblFixDivTab.setText("Fix tab order:");
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Button btnFixTabs = new Button(shlInTools, SWT.NONE);
		btnFixTabs.setLayoutData(new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1));
		btnFixTabs.setText("Fix Tabs");
		btnFixTabs.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(shlInTools, SWT.OPEN);
				String templatesDirectory = dialog.open();				
				
				if(templatesDirectory != null){
					fieldController.reorderFieldDataAndTabs(templatesDirectory);
					
					MessageBox message = new MessageBox(shlInTools, SWT.ICON_QUESTION | SWT.OK| SWT.CANCEL);
					message.setText("Behold My Glory");
					message.setMessage("Extraction complete Mother Fucker!");
					message.open();
				}
			}
		});
		new Label(shlInTools, SWT.NONE);
		
		Label lblExtractFieldNames = new Label(shlInTools, SWT.NONE);
		lblExtractFieldNames.setText("Extract Field Names:");
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Button btnExtract = new Button(shlInTools, SWT.NONE);
		btnExtract.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		btnExtract.setText("Extract");
		btnExtract.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(shlInTools, SWT.OPEN);
				xmlFilepath = dialog.open();				
				
				if(xmlFilepath != null){
					fieldController.exportFieldNamesList(xmlFilepath);
					
					MessageBox message = new MessageBox(shlInTools, SWT.ICON_QUESTION | SWT.OK| SWT.CANCEL);
					message.setText("Behold My Glory");
					message.setMessage("Extraction complete Mother Fucker!");
					message.open(); 
				}
			}
		});
		new Label(shlInTools, SWT.NONE);
		
		Label lblCreateDataDictionary = new Label(shlInTools, SWT.NONE);
		lblCreateDataDictionary.setText("Create Data Dictionary");
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		new Label(shlInTools, SWT.NONE);
		
		Button btnCreate = new Button(shlInTools, SWT.NONE);
		btnCreate.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		btnCreate.setText("Create");
		btnCreate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DirectoryDialog dialog = new DirectoryDialog(shlInTools, SWT.OPEN);
				String landingPagesDirectory = dialog.open();
				String signingTemplatesDirectory = dialog.open();
				
				if((landingPagesDirectory != null) && (signingTemplatesDirectory != null)){
					fieldController.createDataDictionary(landingPagesDirectory, signingTemplatesDirectory);
					
					MessageBox message = new MessageBox(shlInTools, SWT.ICON_QUESTION | SWT.OK| SWT.CANCEL);
					message.setText("Behold My Glory");
					message.setMessage("Extraction complete Mother Fucker!");
					message.open(); 
				}
			}
		});
	}

}
