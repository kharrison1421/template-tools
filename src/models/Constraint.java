package models;

public class Constraint {
	private String groupParent;
	private String groupYTop;
	private String groupXLeft;
	private String groupYBottom;
	private String groupXRight;
	private String transpose;
	private String visibility;
	private String borderColor;
	
	private String lookupName;
	private String lookupFields;
	private String formula;
	private String jsOnChange;
	//=======================================================================================================================================
	public String getGroupParent() {
		return groupParent;
	}
	public void setGroupParent(String groupParent) {
		this.groupParent = groupParent;
	}
	public String getGroupYTop() {
		return groupYTop;
	}
	public void setGroupYTop(String groupYTop) {
		this.groupYTop = groupYTop;
	}
	public String getGroupXLeft() {
		return groupXLeft;
	}
	public void setGroupXLeft(String groupXLeft) {
		this.groupXLeft = groupXLeft;
	}
	public String getGroupYBottom() {
		return groupYBottom;
	}
	public void setGroupYBottom(String groupYBottom) {
		this.groupYBottom = groupYBottom;
	}
	public String getGroupXRight() {
		return groupXRight;
	}
	public void setGroupXRight(String groupXRight) {
		this.groupXRight = groupXRight;
	}
	public String getTranspose() {
		return transpose;
	}
	public void setTranspose(String transpose) {
		this.transpose = transpose;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public String getBorderColor() {
		return borderColor;
	}
	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public String getLookupName() {
		return lookupName;
	}
	public void setLookupName(String lookupName) {
		this.lookupName = lookupName;
	}
	public String getLookupFields() {
		return lookupFields;
	}
	public void setLookupFields(String lookupFields) {
		this.lookupFields = lookupFields;
	}
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	public String getJsOnChange() {
		return jsOnChange;
	}
	public void setJsOnChange(String jsOnChange) {
		this.jsOnChange = jsOnChange;
	}
	//=======================================================================================================================================
}
