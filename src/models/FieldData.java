package models;

import java.io.Serializable;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FieldData implements Serializable{
	private static final long serialVersionUID = -8652410812086966078L;
	
	// Root node
	private Node rootNode;
	
	// Attributes
	private NamedNodeMap attributes;
	private String id;
	private String name;
	private int tabOrder;
	private InputType type;
	
	// Calculated Width
	private double xLeft;
	private double xRight;
	private double yTop;
	private double yBottom;
	
	// Useful children
	private String visibility;
	private String parentDiv;
	private double groupXLeft;
	private double groupXRight;
	private double groupYTop;
	private double groupYBottom;
	
	// Children
	private NodeList children;
	//=======================================================================================================================================
	public Node getRootNode() {
		return this.rootNode;
	}
	public void setRootNode(Element fieldElement) {
		this.rootNode = fieldElement;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public NamedNodeMap getAttributes() {
		return attributes;
	}
	public void setAttributes(NamedNodeMap attributes) {
		this.attributes = attributes;
	}
	public String getId(){
		return id;
	}
	public void setId(String id){
		this.id = id;;
	}
	public int getTabOrder() {
		return tabOrder;
	}
	public void setTabOrder(String tabOrder) {		
		this.tabOrder = Integer.parseInt(tabOrder);
	}
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public InputType getType() {
		return type;
	}
	public void setType(String type) {
		this.type = InputType.getByNumber(Integer.parseInt(type));
	}
	public void setType(InputType type){
		this.type = type;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public double getxLeft() {
		return xLeft;
	}
	public void setxLeft(double xLeft) {		
		this.xLeft = xLeft;
	}
	public double getxRight() {
		return xRight;
	}
	public void setxRight(double xRight) {
		this.xRight = xRight;
	}
	public double getyTop() {
		return yTop;
	}
	public void setyTop(double yTop) {
		this.yTop = yTop;
	}
	public double getyBottom() {
		return yBottom;
	}
	public void setyBottom(double yBottom) {
		this.yBottom = yBottom;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public String getParentDiv() {
		return parentDiv;
	}
	public void setParentDiv(String parentDiv) {
		this.parentDiv = parentDiv;
	}
	public double getGroupXLeft() {
		return groupXLeft;
	}
	public void setGroupXLeft(double groupXLeft) {
		this.groupXLeft = groupXLeft;
	}
	public double getGroupXRight() {
		return groupXRight;
	}
	public void setGroupXRight(double groupXRight) {
		this.groupXRight = groupXRight;
	}
	public double getGroupYTop() {
		return groupYTop;
	}
	public void setGroupYTop(double groupYTop) {
		this.groupYTop = groupYTop;
	}
	public double getGroupYBottom() {
		return groupYBottom;
	}
	public void setGroupYBottom(double groupYBottom) {
		this.groupYBottom = groupYBottom;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public NodeList getChildren() {
		return children;
	}
	public void setChildren(NodeList children) {
		this.children = children;
	}
	//=======================================================================================================================================
}
