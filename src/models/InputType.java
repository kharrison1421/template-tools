package models;

public enum InputType {
	NON_EXISTANT(0), CHECKBOX(2), RADIO_BUTTON(3), TEXT(4), DROPDOWN(6), DATE_PICKER(7), LABEL(8), LINK(9), BOX(10), DIV(12), 
	BUTTON(13), IMAGE(14), ERROR_LIST(15), VALIDATION_WIDGET(16);
	
	private int value;
	//=======================================================================================================================================
	private InputType(int value){
		this.value = value;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public int getValue() {
		return value;
	}
	//---------------------------------------------------------------------------------------------------------------------------------------
	public static InputType getByNumber(int typeNumber) {
	   for (InputType type: values()) {
	      if (type.getValue() == typeNumber) {
	         return type;
	      }
	   }
	   
	   return null;
	}
	//=======================================================================================================================================
}
