package models;

import org.w3c.dom.NamedNodeMap;

public class Page {
	private NamedNodeMap attributes;
	//=======================================================================================================================================
	public NamedNodeMap getAttributes() {
		return attributes;
	}
	public void setAttributes(NamedNodeMap namedNodeMap) {
		this.attributes = namedNodeMap;
	}
	
	//=======================================================================================================================================
}
