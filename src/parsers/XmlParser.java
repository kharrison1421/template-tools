package parsers;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import models.FieldData;
import models.Page;

public class XmlParser {
	private static LinkedHashMap<Page, ArrayList<FieldData>> fieldDataMap;
	private static int maxId = 0;
	// =======================================================================================================================================
	public static LinkedHashMap<Page, ArrayList<FieldData>> parseXML(String filepath) {
		try {
			fieldDataMap = new LinkedHashMap<Page, ArrayList<FieldData>>();

			File inputFile = new File(filepath);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			NodeList pageNodeList = doc.getElementsByTagName("PAGE");
			for (int i = 0; i < pageNodeList.getLength(); i++) {
				Node pageNode = pageNodeList.item(i);
				if (pageNode.getNodeType() == Node.ELEMENT_NODE) {
					Element pageFieldElement = (Element) pageNode;
					Page page = new Page();

					page.setAttributes(pageFieldElement.getAttributes());

					NodeList fieldNodeList = pageFieldElement.getChildNodes();
					ArrayList<FieldData> fieldDataList = new ArrayList<FieldData>();
					for (int j = 0; j < fieldNodeList.getLength(); j++) {
						Node fieldNode = fieldNodeList.item(j);
						if (fieldNode.getNodeType() == Node.ELEMENT_NODE) {
							Element fieldElement = (Element) fieldNode;
							FieldData fieldData = new FieldData();

							// Set root node
							fieldData.setRootNode(fieldElement);

							// All attributes
							fieldData.setAttributes(fieldElement.getAttributes());

							// Extract id integer and set maxId (ex. uField_666 => 666)
							String idString = fieldElement.getAttribute("id");
							int id = Integer.parseInt(idString.replaceAll("[^\\d]", ""));
							fieldData.setId(idString);
							maxId = id > maxId ? id : maxId;

							// Useful attributes
							fieldData.setTabOrder(fieldElement.getAttribute("tabOrder"));
							fieldData.setName(fieldElement.getAttribute("name"));
							fieldData.setType(fieldElement.getAttribute("type"));
							fieldData.setChildren(fieldElement.getChildNodes());

							// Useful children
							fieldData.setxLeft(Double
									.parseDouble(fieldElement.getElementsByTagName("X_LEFT").item(0).getTextContent()));
							fieldData.setxRight(Double.parseDouble(
									fieldElement.getElementsByTagName("X_RIGHT").item(0).getTextContent()));
							fieldData.setyTop(Double
									.parseDouble(fieldElement.getElementsByTagName("Y_TOP").item(0).getTextContent()));
							fieldData.setyBottom(Double.parseDouble(
									fieldElement.getElementsByTagName("Y_BOTTOM").item(0).getTextContent()));

							// Useful constraints
							Node constraints = fieldElement.getElementsByTagName("CONSTRAINTS").item(0);
							Element constraintsElement = (Element) constraints;
							fieldData.setVisibility(
									constraintsElement.getElementsByTagName("VISIBILITY").item(0).getTextContent());
							if (constraintsElement.getElementsByTagName("GROUP_PARENT").item(0) != null) {
								fieldData.setParentDiv(constraintsElement.getElementsByTagName("GROUP_PARENT").item(0)
										.getTextContent());
								fieldData.setGroupXLeft(Double.parseDouble(constraintsElement
										.getElementsByTagName("GROUP_X_LEFT").item(0).getTextContent()));
								fieldData.setGroupXRight(Double.parseDouble(constraintsElement
										.getElementsByTagName("GROUP_X_RIGHT").item(0).getTextContent()));
								fieldData.setGroupYTop(Double.parseDouble(constraintsElement
										.getElementsByTagName("GROUP_Y_TOP").item(0).getTextContent()));
								fieldData.setGroupYBottom(Double.parseDouble(constraintsElement
										.getElementsByTagName("GROUP_Y_BOTTOM").item(0).getTextContent()));
							}

							// Add FieldData to map
							fieldDataList.add(fieldData);
							fieldDataMap.put(page, fieldDataList);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return fieldDataMap;
	}

	public static String extractTemplatename(String filepath) {
		try {
			File inputFile = new File(filepath);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			NodeList templateNodeList = doc
					.getElementsByTagName("router.connector.content.templates.TemplateRepositoryItem");
			Node templateItem = templateNodeList.item(0);

			NodeList children = templateItem.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				Node child = children.item(i);
				if (child.getNodeName().equals("name")) {
					return child.getTextContent();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "";
	}
	// ---------------------------------------------------------------------------------------------------------------------------------------
	public static int getMaxId() {
		return maxId;
	}
	// =======================================================================================================================================
}