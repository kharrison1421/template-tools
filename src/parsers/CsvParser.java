package parsers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class CsvParser {

	private BufferedReader br;
	private String currentLine;
	private String[] splitLine;
	private LinkedHashMap<String, String> lineMap;
	private ArrayList<LinkedHashMap<String, String>> csvData;

	private static final String SPLIT_DELIMITER = ",";
	// =======================================================================================================================================
	public ArrayList<LinkedHashMap<String, String>> parseCSV(String filepath, String[] columnNames) {
		try {
			br = new BufferedReader(new FileReader(filepath));
			csvData = new ArrayList<LinkedHashMap<String, String>>();

			while ((currentLine = br.readLine()) != null) {
				splitLine = currentLine.split(SPLIT_DELIMITER);

				lineMap = new LinkedHashMap<String, String>();
				for (int i = 0; i < columnNames.length; i++) {
					if (columnNames[i].equals("value") || columnNames[i].equals("english")) {
						String value = "";
						for (int j = i; j < splitLine.length; j++) {
							value += splitLine[j];
							value += SPLIT_DELIMITER;
						}

						if (columnNames[i].equals("english"))
							// 3 = Magic number for remove trailing comma and other characters
							value = value.substring(0, value.length() - 3);
						else
							value = value.substring(0, value.length() - SPLIT_DELIMITER.length());

						lineMap.put(columnNames[i], value);
					} else {
						lineMap.put(columnNames[i], splitLine[i]);
					}
				}
				csvData.add(lineMap);
			}
			csvData.remove(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return csvData;
	}
	// =======================================================================================================================================
}